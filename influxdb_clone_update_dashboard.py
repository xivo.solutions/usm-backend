#!/usr/bin/env python3

import json
import os
import re
import requests
from dotenv import load_dotenv
from typing import Dict, List

load_dotenv()

SESSION = requests.Session()
SESSION.headers.update({
    "Authorization": f"Token {os.getenv('DOCKER_INFLUXDB_ADMIN_TOKEN')}"
})

def create_dashboard(name: str, org_id: str, description: str = "") -> Dict:
    """
    Creates a dashboard using the InfluxDB API

    Parameters
    ----------
    name : str
        Name of the dashboard
    org_id : str
        Identifier of the organisation
    description : str, optional
        Description of the dashboard, by default ""

    Returns
    -------
    Dict
        Data of the newly created dashboard
    """

    new_dashboard = requests.Response = SESSION.post(
        f"{os.getenv('USM_INFLUXDB_BASE_URL')}/api/v2/dashboards",
        data=json.dumps({"name": name, "orgID": org_id, "description": description}),
        headers={"Content-Type": "application/json"}

    ).json()

    return new_dashboard


def retrieve_dashboard(dashboard_id: str) -> Dict:
    """
    Gets the content of a dashboard

    Parameters
    ----------
    dashboard_id : str
        Identifier of the dashboard

    Returns
    -------
    Dict
        Content of the dashboard
    """

    dashboard: requests.Response = SESSION.get(
        f"{os.getenv('USM_INFLUXDB_BASE_URL')}/api/v2/dashboards/{dashboard_id}"
    ).json()

    return dashboard


def update_dashboard_xivo_uuid(dashboard: Dict, new_uuid: str) -> Dict:
    """
    Edits the xivo_uuid

    Parameters
    ----------
    dashboard : Dict
        Dashboard to be edited
    new_uuid : str
        XiVO UUID to be applied

    Returns
    -------
    List
        List of updated dashboard cells
    """

    dashboard_cells: List = []

    for cell in dashboard["cells"]:
        cell_content = Dict = SESSION.get(
            f"{os.getenv('USM_INFLUXDB_BASE_URL')}{cell['links']['view']}"
        ).json()
        for query in cell_content["properties"]["queries"]:
            re.sub(
                "(?<=\[\\\"xivo_uuid\\\"\] == \\\")[a-z\d\-]+(?=\\\")",
                new_uuid,
                query["text"]
            )
            for tag in query["builderConfig"]["tags"]:
                if tag["key"] == "xivo_uuid":
                    tag["values"] = [new_uuid]
    
        cell["properties"] = cell_content["properties"] 
        dashboard_cells += [cell]
    
    return dashboard_cells


def populate_dashboard_cells(new_dashboard: Dict, dashboard_cells: List) -> Dict:
    """
    Populates the dashboard with data

    Parameters
    ----------
    new_dashboard : Dict
        Dashboard to fill in
    dashboard_cells : List
        Data to fill in the new dashboard

    Returns
    -------
    Dict
        Updated dashboard
    """

    for cell in dashboard_cells:
        new_cell = SESSION.post(
            f"{os.getenv('USM_INFLUXDB_BASE_URL')}/api/v2/dashboards/{new_dashboard['id']}/cells",
            data=json.dumps({"name": cell["name"], "h": cell["h"], "w": cell["w"], "x": cell["x"], "y": cell["y"]}),
            headers={"Content-Type": "application/json"}
        ).json()

        SESSION.patch(
            f"{os.getenv('USM_INFLUXDB_BASE_URL')}/api/v2/dashboards/{new_dashboard['id']}/cells/{new_cell['id']}/view",
            data=json.dumps({"name": cell["name"], "properties": cell["properties"]}),
            headers={"Content-Type": "application/json"}
        )

    new_dashboard: Dict = retrieve_dashboard(new_dashboard['id'])

    return new_dashboard


def clone_update_dashboard(dashboard_id: str,
                           uuid: str,
                           new_dashboard_name: str,
                           organisation_id: str,
                           new_dashboard_description: str = "") -> Dict:
    """
    Creates a copy of a dashboard and populates the new dashboard
    with updated data

    Parameters
    ----------
    dashboard_id : str
        Identifier of the dashboard to copy
    uuid : str
        UUID of the XiVO machine (to be updated in the new dashboard cells)
    new_dashboard_name : str
        Name of the new dashboard
    organisation_id : str
        Identifier of the organisation of the new dashboard
    new_dashboard_description : str, optional
        Description of the new dashboard, by default ""

    Returns
    -------
    Dict
        New dashboard data
    """

    dashboard: Dict = retrieve_dashboard(dashboard_id)
    dashboard_cells: List = update_dashboard_xivo_uuid(dashboard, uuid)
    new_dashboard: Dict = create_dashboard(new_dashboard_name,
                                     organisation_id,
                                     new_dashboard_description)
    new_dashboard = populate_dashboard_cells(new_dashboard, dashboard_cells)

    return new_dashboard


if __name__ == "__main__":
    dashboard: Dict = clone_update_dashboard(
        os.getenv("USM_INFLUXDB_DASHBOARD_TEMPLATE_ID"),
        os.getenv("USM_INFLUXDB_XIVO_UUID"),
        os.getenv("USM_INFLUXDB_DASHBOARD_NAME"),
        os.getenv("USM_INFLUXDB_DASHBOARD_ORGANISATION_ID"),
        os.getenv("USM_INFLUXDB_DASHBOARD_DESCRIPTION")
    )
