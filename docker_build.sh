#!/usr/bin/env bash
set -e

if [ -z $TARGET_VERSION ]; then
    echo "TARGET_VERSION is not available"
    exit -1
fi

