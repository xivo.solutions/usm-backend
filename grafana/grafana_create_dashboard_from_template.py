#!/usr/bin/env python3

import argparse
import json
import os
import requests
import uuid
from caseconverter import kebabcase
from dotenv import load_dotenv
from pydantic import PositiveInt
from typing import Dict, List

# .env
load_dotenv(".env")

# Requests session
SESSION = requests.Session()
SESSION.headers.update({
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": f"Bearer {os.getenv('USM_GRAFANA_ADMIN_TOKEN')}"
})

# Arguments
parser = argparse.ArgumentParser(
    prog="Grafana - Adds a new client",
    description="Clones a template dashboard and fills it in with the new client details"
)
parser.add_argument("-c", "--client",
                    help="Name of the client")
parser.add_argument("-u", "--uuid",
                    help="UUID of the XiVO machine")
parser.add_argument("-s", "--setpoint",
                    help="Setpoint for the client",
                    type=str)
parser.add_argument("-f", "--folder",
                    help="Unique Identifier of the folder where the dashboard has to be stored")
args = parser.parse_args()


def get_dashboard(dashboard_uid: str) -> Dict:
    """
    Retrieves the template dashboard information from Grafana

    Parameters
    ----------
    dashboard_uid : str
        Unique identifier of the dashboard in Grafana

    Returns
    -------
    Dict
        Data of the template dashboard
    """

    dashboard: Dict = SESSION.get(
        f"{os.getenv('USM_GRAFANA_BASE_URL')}/api/dashboards/uid/{dashboard_uid}"
    ).json()

    return dashboard


def update_variables(template: Dict,
                     client: str,
                     setpoint: PositiveInt,
                     uuid: uuid.UUID) -> List:
    """
    Updates the template variables with the client information

    Parameters
    ----------
    template : Dict
        Data of the template dashboard
    client : str
        Name of the client
    setpoint : PositiveInt
        Number of USM billed to the client
    uuid : uuid.UUID
        UUID of the client's XiVO machine

    Returns
    -------
    List
        Updated variables
    """
    variables: List = template["dashboard"]["templating"]["list"]
    for variable in variables:
        if variable["name"] == "client":
            variable["query"] = client
        elif variable["name"] == "uuid":
            variable["query"] = uuid
        elif variable["name"] == "usm":
            variable["query"] = str(setpoint)
    
    return variables


def create_dashboard(template: Dict, variables: List, folder_uid: str) -> Dict:
    """
    Creates a dashboard in Grafana from the template, the updated variables and
    stores it in the desired folder.

    Parameters
    ----------
    template : Dict
        Data of the template dashboard
    variables : List
        Variables related to the client
    folder_uid : str
        Unique identifier of the folder where the dashboard will be stored

    Returns
    -------
    Dict
        Data of the newly created dashboard
    """

    data: Dict = {
        "dashboard": template["dashboard"],
        "folderUid": folder_uid,
        "message": "Create panel for ${client}",
        "overwrite": True
    }
    del(data["dashboard"]["id"])
    del(data["dashboard"]["uid"])
    data["dashboard"]["title"] = f"USM-{kebabcase(variables[2]['query'])}"
    data["dashboard"]["templating"] = {"list": variables}
    data["dashboard"]["panels"][1]["fieldConfig"]["defaults"]["thresholds"]["steps"] += [{
        "color": "red",
        "value": int(variables[1]["query"])
    }]
    
    dashboard: Dict = SESSION.post(
        f"{os.getenv('USM_GRAFANA_BASE_URL')}/api/dashboards/db",
        data=json.dumps(data)
    ).json()
    return dashboard


def add_new_dashboard(template_uid: str,
                      client: str,
                      setpoint: str,
                      uuid: uuid.UUID,
                      folder_uid: str) -> Dict:
    """
    Creates a new dashboard from a template and client details

    Parameters
    ----------
    template_uid : str
        Unique identifier of the template in Grafana
    client : str
        Name of the client
    setpoint : str
        Number of USM billed to the client
    uuid : uuid.UUID
        UUID of the client's XiVO machine
    folder_uid : str
        Unique identifier of the folder where the dashboard will be stored

    Returns
    -------
    Dict
        Data of the newly created dashboard
    """

    template: Dict = get_dashboard(template_uid)
    variables: List = update_variables(template, client, setpoint, uuid)
    dashboard: Dict = create_dashboard(template, variables, folder_uid)

    return dashboard

if __name__ == "__main__":
    dashboard: Dict = add_new_dashboard(
        os.getenv("USM_GRAFANA_TEMPLATE_UID"),
        args.client,
        args.setpoint,
        args.uuid,
        args.folder
    )
    print(dashboard)
